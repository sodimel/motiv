<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>projet motiv - bienviendue</title>
	<link rel="stylesheet" href="css/design.css" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>

	<section id="left">
		<header>
			<figure>
				<img src="/img/logo-little.png" alt="logo Motiv" />
			</figure>
			<h1>projet motiv</h1>
			<nav>
				<ul>
					<li>Accueil</li>
					<li>Soumettre un projet</li>
					<li>Rejoindre un projet</li>
					<li>Listes
						<ul>
							<li>Projets réalisés</li>
							<li>Projets décédés</li>
						</ul>
					</li>
					<li>À Propos</li>
				</ul>
			</nav>
		</header>

		<footer>
			<p>
				projet motiv
			</p>
			<p>
				une idée <a href="https://bitmycode.com">BitMyCode</a>
			</p>
			<p>
				<a href="https://git.bitmycode.com/sodimel/motiv">fork us on gitlab</a> !
			</p>
		</footer>
	</section>

	<article id="center">
		<header>
			<h2>Projet Motiv</h2>

			présenté par <h3>Corentin Bettiol</h3>
			<p>
				<i>Projet créé le 30 mai 2017.</i>
			</p>
		</header>

		<main>
			<p>
				Alors le Projet Motiv c'est ce site en fait.<br />
				Oui je sais.
			</p>

			<figure>
				<img src="/img/pic1.png" alt="Early-design de cette page." />
				<figcaption>
					Early-design de cette page.
				</figcaption>
			</figure>

			<p>
				Et là du coup cette description est utilisée pour décrire le site qui est aussi le premier projet du site.<br />
				Je vous suis encore ?
			</p>

		</main>

		<footer>
			<p>
				42 encouragements.
			</p>
		</footer>
	</article>

	<section id="right">
		<h3>Commentaires</h3>

		<section>
			<p>
				<b>Corentin Bettiol</b>
			</p>
			<p>
				Ahah c'est rigolo !
			</p>
			<p>
				<i>Posté le 29/11/17 à 20:40:00</i>
			</p>
		</section>

		<section>
			<p>
				<b>Guillaume Charifi</b>
			</p>
			<p>
				you only live once lol
			</p>
			<p>
				<i>Posté le 00/00/00 à 00:00:00:-1</i>
			</p>
		</section>
	</section>

</body>
</html>