<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>projet motiv - bienviendue</title>
	<link rel="stylesheet" href="css/design.css" />
	<meta name="viewport" content="width=device-width" />
</head>
<body>

	<header>
		<h1>projet motiv</h1>
		<nav>
			<ul>
				<li>Accueil</li>
				<li>Soumettre un projet</li>
				<li>Rejoindre un projet</li>
				<li>Listes
					<ul>
						<li>Projets réalisés</li>
						<li>Projets décédés</li>
					</ul>
				</li>
				<li>À Propos</li>
			</ul>
		</nav>
	</header>

	<main>

		<section class="plain">
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce finibus cursus viverra. Fusce fringilla ex eu sagittis fermentum. Ut semper mattis volutpat. Suspendisse vulputate vestibulum cursus. Sed vel blandit massa. In in lectus arcu. Nunc imperdiet cursus quam, at egestas sapien vestibulum et.
			</p>
			<p>
				Donec nec tortor eu sem vehicula vestibulum sit amet eu leo. Suspendisse at magna quis purus volutpat dictum ultrices ut lacus. Nunc eget quam venenatis, bibendum nibh ultrices, suscipit libero. In in eleifend libero. Fusce tempus massa dui, quis mattis urna varius ut. In blandit metus diam, non feugiat ex blandit eget. Sed fermentum arcu id lacus gravida mattis. Proin aliquam facilisis quam non laoreet. Sed maximus mi elit, vitae mollis massa imperdiet eu. Morbi odio massa, pellentesque vitae sapien id, commodo iaculis sapien. Nunc tempus eleifend malesuada. 
			</p>
		</section>

		<section class="container">

			<section class="left">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce finibus cursus viverra. Fusce fringilla ex eu sagittis fermentum. Ut semper mattis volutpat. Suspendisse vulputate vestibulum cursus. Sed vel blandit massa. In in lectus arcu. Nunc imperdiet cursus quam, at egestas sapien vestibulum et.
				</p>
			</section>

			<section class="right">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce finibus cursus viverra. Fusce fringilla ex eu sagittis fermentum. Ut semper mattis volutpat. Suspendisse vulputate vestibulum cursus. Sed vel blandit massa. In in lectus arcu. Nunc imperdiet cursus quam, at egestas sapien vestibulum et.
				</p>
			</section>
		
		</section>

	</main>

	<footer>
		<p>
			projet motiv - une idée <a href="https://bitmycode.com">BitMyCode</a> - <a href="https://git.bitmycode.com/sodimel/motiv">fork us on gitlab</a> !
		</p>
	</footer>

</body>
</html>